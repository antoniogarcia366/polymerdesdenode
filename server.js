//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('Executing polymer from Node on: ' + port);

//base URL, function
app.get('/', function (req, res){

  res.sendFile("index.html", {root: '.'});

});
